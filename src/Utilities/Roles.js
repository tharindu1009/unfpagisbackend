const { getUserId } = require('./genericUtils');
const { getSingleUserProfile } = require('../services/UserServices');


const isAdmin = async (context, userId ) => {
    const user = await context.prisma.user({ id: userId });
    if(user.userType === 'ADMIN') {
        return true;
    }
    return false;
}



module.exports = {
    isAdmin
}