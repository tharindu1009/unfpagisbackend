const jwt = require('jsonwebtoken')
const APP_SECRET = 'ILOMFEDDO'

const getUserId = (context) => {
  const Authorization = context.request.get('Authorization')
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '')
    const { userId } = jwt.verify(token, APP_SECRET)
    return userId
  }

  throw new Error('Not authenticated')
}

const generateMachineName = (string) => {
    let dashed = string.toLowerCase()
      .replace(/\s/g, '-');
    let suffix = Math.floor((Math.random() * 100) + 1);
    return dashed + "-" + suffix;
}

const getUserTypeIDfromMachineName = async (machineName, context) => {
    const usertype = await context.prisma.userType({ machineName });
    //TODO:-- handle this error
    return usertype.id;
}

const titleCase = (str) => {
  let string = str.toLowerCase().split(' ');
  for (var i = 0; i < string.length; i++) {
    string[i] = string[i].charAt(0).toUpperCase() + string[i].slice(1); 
  }
  return string.join(' ');
}

module.exports = {
  APP_SECRET,
  getUserId,
  generateMachineName,
  getUserTypeIDfromMachineName,
  titleCase,
}