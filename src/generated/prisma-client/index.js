"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "User",
    embedded: false
  },
  {
    name: "Setting",
    embedded: false
  },
  {
    name: "PreschoolSubmission",
    embedded: false
  },
  {
    name: "SchoolSubmission",
    embedded: false
  },
  {
    name: "WorkPlaceSubmission",
    embedded: false
  },
  {
    name: "HospitalSubmission",
    embedded: false
  },
  {
    name: "VillageSubmission",
    embedded: false
  },
  {
    name: "PreSchoolEvent",
    embedded: false
  },
  {
    name: "SchoolEvent",
    embedded: false
  },
  {
    name: "WorkPlaceEvent",
    embedded: false
  },
  {
    name: "hospitalEvent",
    embedded: false
  },
  {
    name: "villageEvent",
    embedded: false
  },
  {
    name: "Province",
    embedded: false
  },
  {
    name: "District",
    embedded: false
  },
  {
    name: "GNDivision",
    embedded: false
  },
  {
    name: "MOHArea",
    embedded: false
  },
  {
    name: "PHMArea",
    embedded: false
  },
  {
    name: "UserType",
    embedded: false
  },
  {
    name: "Gender",
    embedded: false
  },
  {
    name: "SettingType",
    embedded: false
  },
  {
    name: "MeetingFrequency",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `https://eu1.prisma.sh/tharindu-dananjaya/unfpagis/dev`
});
exports.prisma = new exports.Prisma();
