const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');

const {
    APP_SECRET,
    getUserId,
    getUserTypeIDfromMachineName } = require('../Utilities/genericUtils');

const {editBasicProfile } = require('../services/UserServices');
const {
    addPreSchoolSettingSubmission,
    addSchoolSettingSubmission,
    addWorkPlaceSettingSubmission,
    addHospitalSettingSubmission,
    addVillageSettingSubmission,
    editPreSchoolSettingSubmission,
    editSchoolSettingSubmission,
    editWorkPlaceSettingSubmission,
    editHospitalSettingSubmission,
    editVillageSettingSubmission
} = require('../services/SettingService');

const {
        addPreSchoolEvent,
        addSchoolEvent,
        addWorkPlaceEvent,
        addHospitalEvent,
        addVillageEvent,
} = require('../services/EventServices');

const signup = async (parent, args, context, info) => {
    const password = await bcrypt.hash(args.password, 10);
    const user = await context.prisma.createUser({
        ...args,
        password
    });
    const token = jwt.sign({ userID: user.id }, APP_SECRET);
    return {
        token,
        user,
    }
}

const login = async (parent, args, context, info) => {
    const user = await context.prisma.user({ nic: args.nic });
    if (!user) {
        throw new Error('No such user found');
    }
    const valid = await bcrypt.compare(args.password, user.password);
    if (!valid) {
        throw new Error('Invalid Password');
    }
    const token = jwt.sign({ userId: user.id }, APP_SECRET);
    return {
        token,
        user,
    }
}

module.exports = {
    signup,
    login,
    editBasicProfile,
    addPreSchoolSettingSubmission,
    addSchoolSettingSubmission,
    addWorkPlaceSettingSubmission,
    addHospitalSettingSubmission,
    addVillageSettingSubmission,
    editPreSchoolSettingSubmission,
    editSchoolSettingSubmission,
    editWorkPlaceSettingSubmission,
    editHospitalSettingSubmission,
    editVillageSettingSubmission,
    addPreSchoolEvent,
    addSchoolEvent,
    addWorkPlaceEvent,
    addHospitalEvent,
    addVillageEvent,
}