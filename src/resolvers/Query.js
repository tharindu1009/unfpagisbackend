const { getAllUsers,getUsersForDistrictConsultant,getSingleUser } = require('../services/UserServices');
const { getAllProvinces,getAllDistricts,getDistrictsForProvince } = require('../services/AreaServices');
const { getAllSettings,
    getSingleSetting,
    getPreSchoolSubmissions,
    getSchoolSubmissions,
    getWorkPlaceSubmissions,
    getHospitalSubmissions,
    getVillageSubmissions } = require('../services/SettingService');

const { 
    getPreSchoolEvents,
    getSchoolEvents,
    getWorkPlaceEvents,
    getHospitalPlaceEvents,
    getVillagePlaceEvents } = require('../services/EventServices');


module.exports = {
    getAllUsers,
    getUsersForDistrictConsultant,
    getSingleUser,
    getAllProvinces,
    getAllDistricts,
    getDistrictsForProvince,
    getAllSettings,
    getSingleSetting,
    getPreSchoolSubmissions,
    getSchoolSubmissions,
    getWorkPlaceSubmissions,
    getHospitalSubmissions,
    getVillageSubmissions,
    getPreSchoolEvents,
    getSchoolEvents,
    getWorkPlaceEvents,
    getHospitalPlaceEvents,
    getVillagePlaceEvents
}