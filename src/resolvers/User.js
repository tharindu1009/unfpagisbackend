const userType = (parent, args, context) => {
    return context.prisma.user({ id: parent.id }).userType();
}

module.exports = {
    userType
}

// This is a comment