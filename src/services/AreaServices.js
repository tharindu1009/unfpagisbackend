const { getUserId } = require('../Utilities/genericUtils');

const getAllProvinces = async (parent, args, context) => {
    return context.prisma.provinces();
}

const getAllDistricts = async (parent, args, context) => {
    return context.prisma.districts();
}

const getDistrictsForProvince = async (parent, args, context) => {
    const where = {provinceId: args.provinceId};
    return context.prisma.districts({
        where: where
    })
}

module.exports = {
    getAllProvinces,
    getAllDistricts,
    getDistrictsForProvince
}

