const { getUserId } = require('../Utilities/genericUtils');

const addPreSchoolEvent = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createPreSchoolEvent({
        ...args,
        createdBy,
    });
}

const addSchoolEvent = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createSchoolEvent({
        ...args,
        createdBy,
    });
}

const addWorkPlaceEvent = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createWorkPlaceEvent({
        ...args,
        createdBy,
    });
}

const addHospitalEvent = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createhospitalEvent({
        ...args,
        createdBy,
    });
}

const addVillageEvent = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createvillageEvent({
        ...args,
        createdBy,
    });
}

const getPreSchoolEvents = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var preSchoolEvents = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        preSchoolEvents = await context.prisma.preSchoolEvents({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        preSchoolEvents = await context.prisma.preSchoolEvents({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.preSchoolEventsConnection({ where }).aggregate().count();
    return {
        preSchoolEvents: preSchoolEvents,
        count: settingCount,
    }
}

const getSchoolEvents = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var schoolEvents = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        schoolEvents = await context.prisma.schoolEvents({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        schoolEvents = await context.prisma.schoolEvents({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.schoolEventsConnection({ where }).aggregate().count();
    return {
        schoolEvents: schoolEvents,
        count: settingCount,
    }
}

const getWorkPlaceEvents = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var workPlaceEvents = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        workPlaceEvents = await context.prisma.workPlaceEvents({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        workPlaceEvents = await context.prisma.workPlaceEvents({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.workPlaceEventsConnection({ where }).aggregate().count();
    return {
        workPlaceEvents: workPlaceEvents,
        count: settingCount,
    }
}

const getHospitalPlaceEvents = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var hospitalEvents = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        hospitalEvents = await context.prisma.hospitalEvents({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        hospitalEvents = await context.prisma.hospitalEvents({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.hospitalEventsConnection({ where }).aggregate().count();
    return {
        hospitalEvents: hospitalEvents,
        count: settingCount,
    }
}

const getVillagePlaceEvents = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var villageEvents = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        villageEvents = await context.prisma.villageEvents({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        villageEvents = await context.prisma.villageEvents({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.villageEventsConnection({ where }).aggregate().count();
    return {
        villageEvents: villageEvents,
        count: settingCount,
    }
}

module.exports = {
    addPreSchoolEvent,
    addSchoolEvent,
    addWorkPlaceEvent,
    addHospitalEvent,
    addVillageEvent,
    getPreSchoolEvents,
    getSchoolEvents,
    getWorkPlaceEvents,
    getHospitalPlaceEvents,
    getVillagePlaceEvents
}