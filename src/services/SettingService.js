const { getUserId } = require('../Utilities/genericUtils');

const getAllSettings = async (parent, args, context) => {
    return context.prisma.settings();
}

const getSingleSetting = async (parent, args, context) => {
    return await context.prisma.setting({ id: args.settingId });
}

const addPreSchoolSettingSubmission = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createPreschoolSubmission({
        ...args,
        createdBy,
    });
}

const addSchoolSettingSubmission = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createSchoolSubmission({
        ...args,
        createdBy,
    });
}

const addWorkPlaceSettingSubmission = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createWorkPlaceSubmission({
        ...args,
        createdBy,
    });
}

const addHospitalSettingSubmission = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createHospitalSubmission({
        ...args,
        createdBy,
    });
}

const addVillageSettingSubmission = async (parent, args, context) => {
    const createdBy = getUserId(context);
    return await context.prisma.createVillageSubmission({
        ...args,
        createdBy,
    });
}

const editPreSchoolSettingSubmission = async (parent, args, context) => {
    const updatededBy = getUserId(context);
    const updateSettingSubmission = await context.prisma.updatePreschoolSubmission({
        data: {
            provinceId: args.provinceId,
            Province: args.Province,
            districtId: args.districtId,
            District: args.District,
            MOHArea: args.MOHArea,
            MOHAreaId: args.MOHAreaId,
            PHMArea: args.PHMArea,
            PHMAreaId: args.PHMAreaId,
            gndivision: args.gndivision,
            gndivisionId: args.gndivisionId,
            Location: args.Location,
            MSG: args.MSG,
            FacilitatingPHM: args.FacilitatingPHM,
            MSGPresident: args.MSGPresident,
            MSGPresidentTel: args.MSGPresidentTel,
            EstablishmentAt: args.EstablishmentAt,
            NumOfMembers: args.NumOfMembers,
            NumOfMaleMembers: args.NumOfMaleMembers,
            MeetingFrequency: args.MeetingFrequency,
            NumOfMeetingInYear: args.NumOfMeetingInYear,
            comments: args.comments,
            isActive: args.isActive,
            approvedBy: args.updatededBy
        },
        where: { id: args.settingSubmissionId }

    });
    return updateSettingSubmission;
}

const editSchoolSettingSubmission = async (parent, args, context) => {
    const updatededBy = getUserId(context);
    const updateSettingSubmission = await context.prisma.updateSchoolSubmission({
        data: {
            provinceId: args.provinceId,
            Province: args.Province,
            districtId: args.districtId,
            District: args.District,
            MOHArea: args.MOHArea,
            MOHAreaId: args.MOHAreaId,
            PHMArea: args.PHMArea,
            PHMAreaId: args.PHMAreaId,
            gndivision: args.gndivision,
            gndivisionId: args.gndivisionId,
            Location: args.Location,
            MSG: args.MSG,
            FacilitatingPHM: args.FacilitatingPHM,
            MSGPresident: args.MSGPresident,
            MSGPresidentTel: args.MSGPresidentTel,
            EstablishmentAt: args.EstablishmentAt,
            NumOfMembers: args.NumOfMembers,
            NumOfMaleMembers: args.NumOfMaleMembers,
            MeetingFrequency: args.MeetingFrequency,
            NumOfMeetingInYear: args.NumOfMeetingInYear,
            comments: args.comments,
            isActive: args.isActive,
            approvedBy: args.updatededBy
        },
        where: { id: args.settingSubmissionId }

    });
    return updateSettingSubmission;
}

const editWorkPlaceSettingSubmission = async (parent, args, context) => {
    const updatededBy = getUserId(context);
    const updateSettingSubmission = await context.prisma.updateWorkPlaceSubmission({
        data: {
            provinceId: args.provinceId,
            Province: args.Province,
            districtId: args.districtId,
            District: args.District,
            MOHArea: args.MOHArea,
            MOHAreaId: args.MOHAreaId,
            PHMArea: args.PHMArea,
            PHMAreaId: args.PHMAreaId,
            gndivision: args.gndivision,
            gndivisionId: args.gndivisionId,
            Location: args.Location,
            MSG: args.MSG,
            FacilitatingPHM: args.FacilitatingPHM,
            MSGPresident: args.MSGPresident,
            MSGPresidentTel: args.MSGPresidentTel,
            EstablishmentAt: args.EstablishmentAt,
            NumOfMembers: args.NumOfMembers,
            NumOfMaleMembers: args.NumOfMaleMembers,
            MeetingFrequency: args.MeetingFrequency,
            NumOfMeetingInYear: args.NumOfMeetingInYear,
            comments: args.comments,
            isActive: args.isActive,
            approvedBy: args.updatededBy
        },
        where: { id: args.settingSubmissionId }

    });
    return updateSettingSubmission;
}


const editHospitalSettingSubmission = async (parent, args, context) => {
    const updatededBy = getUserId(context);
    const updateSettingSubmission = await context.prisma.updateHospitalSubmission({
        data: {
            provinceId: args.provinceId,
            Province: args.Province,
            districtId: args.districtId,
            District: args.District,
            MOHArea: args.MOHArea,
            MOHAreaId: args.MOHAreaId,
            PHMArea: args.PHMArea,
            PHMAreaId: args.PHMAreaId,
            gndivision: args.gndivision,
            gndivisionId: args.gndivisionId,
            Location: args.Location,
            MSG: args.MSG,
            FacilitatingPHM: args.FacilitatingPHM,
            MSGPresident: args.MSGPresident,
            MSGPresidentTel: args.MSGPresidentTel,
            EstablishmentAt: args.EstablishmentAt,
            NumOfMembers: args.NumOfMembers,
            NumOfMaleMembers: args.NumOfMaleMembers,
            MeetingFrequency: args.MeetingFrequency,
            NumOfMeetingInYear: args.NumOfMeetingInYear,
            comments: args.comments,
            isActive: args.isActive,
            approvedBy: args.updatededBy
        },
        where: { id: args.settingSubmissionId }

    });
    return updateSettingSubmission;
}

const editVillageSettingSubmission = async (parent, args, context) => {
    const updatededBy = getUserId(context);
    const updateSettingSubmission = await context.prisma.updateVillageSubmission({
        data: {
            provinceId: args.provinceId,
            Province: args.Province,
            districtId: args.districtId,
            District: args.District,
            MOHArea: args.MOHArea,
            MOHAreaId: args.MOHAreaId,
            PHMArea: args.PHMArea,
            PHMAreaId: args.PHMAreaId,
            gndivision: args.gndivision,
            gndivisionId: args.gndivisionId,
            Location: args.Location,
            MSG: args.MSG,
            FacilitatingPHM: args.FacilitatingPHM,
            MSGPresident: args.MSGPresident,
            MSGPresidentTel: args.MSGPresidentTel,
            EstablishmentAt: args.EstablishmentAt,
            NumOfMembers: args.NumOfMembers,
            NumOfMaleMembers: args.NumOfMaleMembers,
            MeetingFrequency: args.MeetingFrequency,
            NumOfMeetingInYear: args.NumOfMeetingInYear,
            comments: args.comments,
            isActive: args.isActive,
            approvedBy: args.updatededBy
        },
        where: { id: args.settingSubmissionId }

    });
    return updateSettingSubmission;
}

const getPreSchoolSubmissions = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var preschoolSettingSubmissions = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        preschoolSettingSubmissions = await context.prisma.preschoolSubmissions({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        preschoolSettingSubmissions = await context.prisma.preschoolSubmissions({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.preschoolSubmissionsConnection({ where }).aggregate().count();
    return {
        preschoolSettingSubmissions: preschoolSettingSubmissions,
        count: settingCount,
    }
}

const getSchoolSubmissions = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var schoolSettingSubmissions = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        schoolSettingSubmissions = await context.prisma.schoolSubmissions({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        schoolSettingSubmissions = await context.prisma.schoolSubmissions({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.schoolSubmissionsConnection({ where }).aggregate().count();
    return {
        schoolSettingSubmissions: schoolSettingSubmissions,
        count: settingCount,
    }
}

const getWorkPlaceSubmissions = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var workPlaceSettingSubmissions = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        workPlaceSettingSubmissions = await context.prisma.workPlaceSubmissions({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        workPlaceSettingSubmissions = await context.prisma.workPlaceSubmissions({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.workPlaceSubmissionsConnection({ where }).aggregate().count();
    return {
        workPlaceSettingSubmissions: workPlaceSettingSubmissions,
        count: settingCount,
    }
}

const getHospitalSubmissions = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var hospitalSettingSubmissions = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        hospitalSettingSubmissions = await context.prisma.hospitalSubmissions({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        hospitalSettingSubmissions = await context.prisma.hospitalSubmissions({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.hospitalSubmissionsConnection({ where }).aggregate().count();
    return {
        hospitalSettingSubmissions: hospitalSettingSubmissions,
        count: settingCount,
    }
}

const getVillageSubmissions = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    const userType = await context.prisma.user({ id: userId }).userType();

    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }

    var villageSettingSubmissions = [];
    if(userType=="HEO" || userType=="RDHS" || userType=="CCP" || userType=="HGO"){
        villageSettingSubmissions = await context.prisma.villageSubmissions({
            where: where,
            skip: args.skip,
            first: args.first,
        })
    }else{
        villageSettingSubmissions = await context.prisma.villageSubmissions({
            skip: args.skip,
            first: args.first,
        })
    }
    
    const settingCount = await context.prisma.villageSubmissionsConnection({ where }).aggregate().count();
    return {
        villageSettingSubmissions: villageSettingSubmissions,
        count: settingCount,
    }
}

module.exports = {
    getAllSettings,
    getSingleSetting,
    addPreSchoolSettingSubmission,
    addSchoolSettingSubmission,
    addWorkPlaceSettingSubmission,
    addHospitalSettingSubmission,
    addVillageSettingSubmission,
    getPreSchoolSubmissions,
    getSchoolSubmissions,
    getWorkPlaceSubmissions,
    getHospitalSubmissions,
    getVillageSubmissions,
    editPreSchoolSettingSubmission,
    editSchoolSettingSubmission,
    editWorkPlaceSettingSubmission,
    editHospitalSettingSubmission,
    editVillageSettingSubmission
}

