const bcrypt = require('bcryptjs');
const { getUserId } = require('../Utilities/genericUtils');


const { isAdmin } = require('../Utilities/Roles');


const getAllUsers = async (parent, args, context) => {
    const userId = getUserId(context);
    const where = args.userType ? { userType: args.userType } : {};
    const users = await context.prisma.users({
        where,
        skip: args.skip,
        first: args.first,
    });
    const userCount = await context.prisma.usersConnection({ where }).aggregate().count();
    return {
        users: users,
        count: userCount,
    }
}

const editBasicProfile = async (parent, args, context) => {
    const userId = getUserId(context);
    const updatedUserProfile = await context.prisma.updateUser({
        data: {
            firstName: args.firstName,
            lastName: args.lastName,
            tel: args.tel,
            province: args.tel,
            district: args.district,
            division: args.division,
            location: args.location,
            email: args.email,
            gender: args.gender,
            password: args.password,
            isActive: args.isActive,
            approvedBy: args.isActive ? userId : ''
        },
        where: {
            id: args.userId,
        }
    });
    return updatedUserProfile

}

const getUsersForDistrictConsultant = async (parent, args, context) => {
    const userId = getUserId(context);
    const districtId = await context.prisma.user({ id: userId }).districtId();
    var where = {};
    if(districtId!=null && districtId!=""){
        where = {districtId: districtId}
    }
    const users = await context.prisma.users({
        where: where,
        skip: args.skip,
        first: args.first,
    })
    const userCount = await context.prisma.usersConnection({ where }).aggregate().count();
    return {
        users: users,
        count: userCount,
    }
}

const getSingleUser = async (parent, args, context) => {
    return await context.prisma.user({ id: args.userId });
}


module.exports = {
    getAllUsers,
    getUsersForDistrictConsultant,
    editBasicProfile,
    getSingleUser
}